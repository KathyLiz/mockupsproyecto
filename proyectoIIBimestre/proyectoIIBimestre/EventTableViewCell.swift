//
//  EventTableViewCell.swift
//  proyectoIIBimestre
//
//  Created by Katherine Hurtado on 19/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var priceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var descriptionHeighContraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var platoLabel: UILabel!
    
    @IBOutlet weak var bebidaLabel: UILabel!
    
    
    @IBOutlet weak var discoMovilLabel: UILabel!
    
    @IBOutlet weak var personasLabel: UILabel!
    
    @IBOutlet weak var descHeight: NSLayoutConstraint!
    
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var bebidaheight: NSLayoutConstraint!
    
    @IBOutlet weak var discoheight: NSLayoutConstraint!
    
    @IBOutlet weak var nPersonasHeight: NSLayoutConstraint!
    
    @IBAction func solicitarButtonPressed(_ sender: Any) {
        
    }
    
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    
   /* var isExpanded:Bool = false
    {
        didSet
        {
            if !isExpanded {
                self.imgHeightConstraint.constant = 0
                self.buttonHeightConstraint.constant = 0
                self.descHeight.constant=0
                self.menuHeight.constant=0
                self.bebidaheight.constant=0
                self.discoheight.constant=0
                self.nPersonasHeight.constant=0
             //   self.descriptionHeighContraint.constant=0
            //   self.priceHeightConstraint.constant=0
                
            } else {
                self.imgHeightConstraint.constant = 138.0
                self.buttonHeightConstraint.constant = 30.0
                //self.descriptionHeighContraint.constant=128.0
                self.priceHeightConstraint.constant=21.0
                self.descHeight.constant=21.0
                self.menuHeight.constant=21.0
                self.bebidaheight.constant=21.0
                self.discoheight.constant=21.0
                self.nPersonasHeight.constant=21.0
            }
        }
    }*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillEvento(_ eventoRec:EventoRecomendado){
       // priceLabel.text = String(describing: eventoRec.precio)
        titleLabel.text = eventoRec.nombre;

    }


}
