//
//  EventoRecomendadoViewController.swift
//  proyectoIIBimestre
//
//  Created by Katherine Hurtado on 24/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import AlamofireImage


class EventoRecomendadoViewController: UIViewController {

    var eventoRec : EventoRecomendado!
    var menu : Menu!
    var bebidas : Bebidas!
    
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var precio: UILabel!
    
    
    @IBOutlet weak var desc: UILabel!
    
    @IBOutlet weak var plato: UILabel!
    @IBOutlet weak var nPersonas: UILabel!
    @IBOutlet weak var bebida: UILabel!
    @IBOutlet weak var discoMovil: UILabel!
    
    @IBAction func solicitarevento(_ sender: Any) {
        let alert = UIAlertController(title: "Información", message: "¿Ya tiene una cuenta en nuestra APP?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "RegistroViewController") as! RegistroViewController
                self.show(vc, sender: self)
                
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.show(vc, sender: self)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Eventos de Temporada"
        fillImageUrl(id: eventoRec.imagen!){
            (pkImage) in self.img.image = pkImage
        }
        titulo.text = eventoRec.nombre!
        precio.text = "$ \(String (describing: eventoRec.precio))"
        plato.text = "Menu: \(menu.nombre!)"
        bebida.text = "Bebida: Exquisito \(bebidas.nombre!)"
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillImageUrl (id: String, completion:@escaping (UIImage)->()){
        let url = id
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completion(image)
            }
        }
        
    }
    

}
