//
//  EventosSinLoginViewController.swift
//  proyectoIIBimestre
//
//  Created by Katherine Hurtado on 19/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage

class EventosSinLoginViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var expandedRows = Set<Int>()

    private let manageObjectContex = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var eRArray : [EventoRecomendado] = []
    var bebidasArray : [Bebidas] = []
    var menuArray : [Menu] = []
    var selectedEvent = 0
    
    override func viewDidLoad() {
        title="Eventos de Temporada"
        super.viewDidLoad()
     //    insertData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        getData ()
        getDataBebidas ()
       getDataMenu ()
       // deleteData ()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func insertData (){
        let uuid = UUID();
        let entityDescription = NSEntityDescription.entity(forEntityName: "EventoRecomendado", in: manageObjectContex)
        let entityDescriptionBebidas = NSEntityDescription.entity(forEntityName: "Bebidas", in: manageObjectContex)
        let entityDescriptionMenu = NSEntityDescription.entity(forEntityName: "Menu", in: manageObjectContex)
        //Se define una operación a ejecutarse ne la bdd con el nombre de la clase
        let eventoRec = EventoRecomendado(entity: entityDescription!, insertInto: manageObjectContex)
        let bebidas = Bebidas(entity: entityDescriptionBebidas!, insertInto: manageObjectContex)
        let menu = Menu(entity: entityDescriptionMenu!, insertInto: manageObjectContex)
        
        eventoRec.id_eventoRecomendado = uuid
        eventoRec.nombre = "Fin de Año"
        eventoRec.imagen = "https://www.ip.gov.py/ip/wp-content/uploads/2016/12/cena-de-navidad_comida.jpg"
        eventoRec.precio = 450
        bebidas.id_bebida = uuid
        bebidas.nombre = "Vino Hervido"
        bebidas.descripcion = "Agua Ardiente + Naranjilla y Canela"
        bebidas.precio = 25
        bebidas.tipo = "Alcohólica"
        menu.id_plato = uuid
        menu.descripcion="Carne de cerdo, choclo, tortillas de papa, mote, maduro frito, ensalada, ajì"
        menu.nombre="Pierna de cerdo"
        menu.precio = 4.5
        menu.tipo = "Principal"
        eventoRec.addToEventoRec_bebidas(bebidas)
        eventoRec.addToEventoRec_menu(menu)
        
        
        //Bloque try y catch para hacer el commit
        do {
            try manageObjectContex.save()
        } catch  {
            print("Error")
        }
    }
    
    func deleteData (){
         //let entityDescription = NSEntityDescription.entity(forEntityName: "EventoRecomendado", in: manageObjectContex)
        
        for index in 1...5{
            manageObjectContex.delete(eRArray[index] as NSManagedObject)
            eRArray.remove(at: index)
            //Bloque try y catch para hacer el commit
            do {
                try manageObjectContex.save()
                print ("Delete OK")
            } catch  {
                print("Error")
            }
        }
        
        
    }
    
    func getData (){
        eRArray=[]
        let request:NSFetchRequest<EventoRecomendado> = EventoRecomendado.fetchRequest()
        do {
            let resultSet = try manageObjectContex.fetch(request)
            for p in resultSet{
                if(p.nombre != nil && p.imagen != nil && p.precio != nil){
                    eRArray.append(p as! EventoRecomendado)
                    print ("Nombre: \(p.nombre!) -- ",terminator:"")
                    print ("Precio: \(p.precio!) -- ",terminator:"")
                    print ("Imagen: \(p.imagen!) ",terminator:"")
                    //Esto es un array
                    print ("Bebida: \(type(of: p.eventoRec_bebidas)))",terminator:"")
                }else{
                    print ("existen valores nulos en Respuesta\(resultSet.index(after: 0))")
                }
            }
           // performSegue(withIdentifier: "showAllPersons", sender: self)
        } catch let error{
            print ("Error al consultar\(error)")
        }
    }
    
    func getDataBebidas (){
        bebidasArray=[]
        let request:NSFetchRequest<Bebidas> = Bebidas.fetchRequest()
        do {
            let resultSet = try manageObjectContex.fetch(request)
            for p in resultSet{
                bebidasArray.append(p as! Bebidas)
                
                //Imprime con saltos de línea
                 print ("Nombre bebida: \(p.nombre!) -- ",terminator:"")
                print ("Precio bebida: \(p.precio!) -- ",terminator:"")
                print ("Tipo bebida: \(p.tipo!) -- ",terminator:"")
            }
            // performSegue(withIdentifier: "showAllPersons", sender: self)
        } catch let error{
            print ("Error al consultar\(error)")
        }
    }
    
    func getDataMenu (){
        menuArray=[]
        let request:NSFetchRequest<Menu> = Menu.fetchRequest()
        do {
            let resultSet = try manageObjectContex.fetch(request)
            for p in resultSet{
                menuArray.append(p as! Menu)
                
                //Imprime con saltos de línea
                print ("Nombre menu: \(p.nombre!) -- ",terminator:"")

            }
            // performSegue(withIdentifier: "showAllPersons", sender: self)
        } catch let error{
            print ("Error al consultar\(error)")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    // cuantas filas va a tener la seccion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return eRArray.count;
        default:
            return 2;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //instanciar la celda personalizada
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpandableCell") as! EventTableViewCell
        cell.fillEvento(eRArray[indexPath.row])
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedEvent = indexPath.row
        performSegue(withIdentifier: "showDetail",
                     sender: self)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedEvent = indexPath.row
        print(indexPath.row)
        return indexPath
    }   
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Se hace cast a la clase a la que se está yendo, esta retorna un view controller
        if segue.identifier == "showDetail" {
        let destination = segue.destination as! EventoRecomendadoViewController
        destination.eventoRec =  eRArray[selectedEvent]
         destination.menu =  menuArray[selectedEvent]
         destination.bebidas =  bebidasArray[selectedEvent]
        }
    }

}
