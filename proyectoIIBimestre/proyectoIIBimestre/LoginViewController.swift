//
//  LoginViewController.swift
//  proyectoIIBimestre
//
//  Created by Katherine Hurtado on 9/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import CoreData
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var claveTextField: UITextField!
    
    var user:Usuario?
    private let manageObjectContex = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func loginButtonPressed(_ sender: Any) {
        
        
        fetchOne()
    }
    
    func fetchOne(){
        
        let request:NSFetchRequest<Usuario> = Usuario.fetchRequest()
        
        let pred = NSPredicate(format:"nombre = %@ AND clave = %@",nombreTextField.text!,claveTextField.text!)
        request.predicate=pred
        
        do {
            let results = try manageObjectContex.fetch(request)
            if results.count > 0{
                user = results[0]
                let username = self.user?.nombre
                let password = self.user?.clave
                print(username!)
                print(password!)
                switch (username!, password!) {
                case (username!,password!):
                    let alert = UIAlertController(title: "Bienvenido", message: user?.nombre, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
                        switch action.style{
                        case .default:
                            
                                print("default")
                                self.performSegue(withIdentifier: "showSesionIniciada", sender: self)
                           
                            
                        case .cancel:
                            print("cancel")
                        case .destructive:
                            print("destructive")
                        }}))
                    self.present(alert, animated: true, completion: nil)
                case (username!, _):
                    self.showAlert(message: "contraseña incorrecta")
                    print ("contraseña incorrecta")
                default:
                    self.showAlert(message: "usuario y contraseña incorrectas")
                    print("usuario y contraseña incorrectas")
                    
                }
                // phoneTextField.text = persona.phone?
                // addressTextField.text = persona.address?
               //performSegue(withIdentifier: "findOneSegue", sender: self)
            }
        } catch let error {
            print ("Error al consultar\(error)")
        }
    }
    
    private func showAlert(message:String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .actionSheet)
        
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.nombreTextField.text = ""
            self.claveTextField.text = ""
        }
    }
    
    func showAlertDialog(){
        let alert = UIAlertController(title: "Bienvenida", message: "Kathy", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.performSegue(withIdentifier: "showLogin", sender: self)
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
}
