//
//  RegistroViewController.swift
//  proyectoIIBimestre
//
//  Created by Katherine Hurtado on 9/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class RegistroViewController: UIViewController, UNUserNotificationCenterDelegate {

    var user:Usuario?
    private let manageObjectContex = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var validacionesLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func registroButtonPressed(_ sender: Any) {
        //Relación entre la entidad de CoreData y la clase que se genera en el código
        let entityDescription = NSEntityDescription.entity(forEntityName: "Usuario", in: manageObjectContex)
        
        //Se define una operación a ejecutarse ne la bdd con el nombre de la clase
        let usuario = Usuario(entity: entityDescription!, insertInto: manageObjectContex)
        let uuid = UUID();
        if(isValidPassword(passwordTextField.text!)){
            if(validarDatos()){
                usuario.idUsuario = uuid;
                usuario.nombre = userNameTextField.text!
                usuario.correo = emailTextField.text!
                usuario.clave = passwordTextField.text!
                //Bloque try y catch para hacer el commit
                do {
                    try manageObjectContex.save()
                    let alert = UIAlertController(title: "Registro Exitoso", message: "Eventos Wilson", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
                        switch action.style{
                        case .default:
                            print("default")
                            self.performSegue(withIdentifier: "showLogin", sender: self)
                        case .cancel:
                            print("cancel")
                        case .destructive:
                            print("destructive")
                        }}))
                    self.present(alert, animated: true, completion: nil)

                    limpiarCampos()
                } catch  {
                    print("Error")
                }
            }else{
               
                validacionesLabel.text = "Las contraseñas no coinciden"
                limpiarContraseñas()
            }
        }else{
            let alert = UIAlertController(title: "Información", message: "La contraseña ingresada debe ser mayor a 8 caracteres, debe incluir al menos una letra mayúsucla, números y caracteres especiales", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            limpiarContraseñas()
        }
    }
    
    func limpiarCampos(){
        userNameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    func limpiarContraseñas(){
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    func validarDatos() -> Bool {
        if(passwordTextField.text == confirmPasswordTextField.text){
            return true
        }else{
            validacionesLabel.text = "Las contraseñas no coinciden"
            return false
        }
    }
    
    public func isValidPassword(_ password : String) -> Bool {
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
}
