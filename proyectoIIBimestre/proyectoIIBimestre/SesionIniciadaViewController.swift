//
//  SesionIniciadaViewController.swift
//  proyectoIIBimestre
//
//  Created by Freddy Bazante on 26/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit
import CoreData
class SesionIniciadaViewController: UIViewController {

    
    @IBOutlet weak var usuarioLabel: UILabel!
    
    var user:Usuario?
    private let manageObjectContex = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchOne()
        
    }
    
    func fetchOne (){
        
        let request:NSFetchRequest<Usuario> = Usuario.fetchRequest()
        
        
        do {
            let results = try manageObjectContex.fetch(request)
            if results.count > 0{
                user = results[0]
                let username = self.user?.nombre
                usuarioLabel.text = username
                print(username)
            }
        } catch let error {
            print ("Error al consultar\(error)")
        }
    }
   

}
