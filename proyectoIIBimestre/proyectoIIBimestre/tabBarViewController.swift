//
//  TabBarViewController.swift
//  proyectoIIBimestre
//
//  Created by Freddy Bazante on 26/2/18.
//  Copyright © 2018 Katherine Hurtado. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
        let items = tabBar.items!
        
        items[0].title = "Home"
        //items[0].image = #imageLiteral(resourceName: "ic_extension")
        
        items[1].title = "Eventos Solicitados"
        //items[1].image = #imageLiteral(resourceName: "ic_date_range")
        
        items[2].title = "Eventos de Temporada"
        //items[1].image = #imageLiteral(resourceName: "ic_date_range")
        
        items[3].title = "Solicitar evento"
        //items[1].image = #imageLiteral(resourceName: "ic_date_range")
        
    }

}
